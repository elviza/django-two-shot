from django.urls import path
from receipts.views import (
    create_receipt,
    receipt_list,
    category_list,
    account_list,
    category_create,
    create_account,
)

urlpatterns = [
    path("", receipt_list, name="home"),
    path("create/", create_receipt, name="create_receipt"),
    path("categories/", category_list, name="category_list"),
    path("accounts/", account_list, name="account_list"),
    path("categories/create/", category_create, name="category_create"),
    path("accounts/create/", create_account, name="create_account"),
]
